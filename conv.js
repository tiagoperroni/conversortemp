
var btnConverter = document.getElementById("btnConverter")

btnConverter.onclick = function (event) {
    let tempEntrada = Number(document.getElementById("idTemp").value)
    //erro aqui adicionei o # para identificar que é um id
    let unidadeEntrada = document.querySelector("#idUnidadeOrigem").value
    let unidadeConvesao = document.querySelector("#idUnidadeConvertido").value
    event.preventDefault();

    //Conversao da unidade de entrada para Celsius 
    let tempCesius

    switch (unidadeEntrada) {
        case "C":
            tempCesius = tempEntrada
            break;

        case "F":
            tempCesius = (tempEntrada * 5 - 160) / 9
            break;
            //faltava o breake acima
        case "K":
            tempCesius = tempEntrada - 273
            break;

        default:
            break;
    }

    //Conversao de celsius para a unidade de saida 
    let tempConvertido = tempCesius 
    switch (unidadeConvesao) {
        case "C":
            tempConvertido = tempCesius
            break;
        case "F":
            tempConvertido = (9 * tempCesius + 160) / 5
            break;
        case "K":
            tempConvertido = tempCesius + 273
            break;
        default:
            break;
    }

    document.getElementById("idTempConvertido").value = tempConvertido
}


// #1- Link do script estava no lugar errado
// #2- Faltava o # em #idUnidadeOrigem
// #3- faltava um breake na linha 21